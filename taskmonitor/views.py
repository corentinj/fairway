from django.shortcuts import render
from django.views import generic
from taskmonitor.models import ComponentGroup
from django.http import Http404
import json

# Create your views here.
# Serialize Components Tree to JSON

def mptt_tree(root_nodes_queryset):

    tree =[]

    def loop(node):
        node_list = []
        for child in node.children.all() :
            node_dic = {'text': child.name}
            if not child.is_leaf_node():
                node_dic['nodes'] = loop(child)
            node_list.append(node_dic)
        return node_list
            
    for root in root_nodes_queryset:
        dic = {'text': root.name}
        if not root.is_leaf_node():
            dic['nodes'] = loop(root)
        tree.append(dic)

    return tree

def task_last_done_date(task, component_group):
    last_tasklog = component_group.tasklogentry_set.filter(task=task).order_by('date').first()
    try:
        return last_tasklog.date
    except AttributeError:
        return "Never"


def task_next_due_date(task, component_group):
    # FIXME Can't convert 'datetime.timedelta' object to str implicitly
    return task.last_done_date(component_group) + task.interval


def each_context():
    """
    Returns a dictionary of variables to put in the template context for
    *every* page in the site.
    """
    
    return {
            'nodes':ComponentGroup.tree.filter(level__lte=2)
            }



class IndexView(generic.ListView):
    model = ComponentGroup
    template_name = 'taskmonitor/base_taskmonitor.html'
    context_object_name = 'groups'

    def get_context_data(self, ** kwargs):
        context = super(IndexView, self).get_context_data(** kwargs)
        context.update(each_context())
        return context


def detail(request, pk):

    try:
        component_group = ComponentGroup.objects.get(pk=pk)
    except ComponentGroup.DoesNotExist:
        raise Http404("Component does not exist")

    context = {'componentgroup': component_group}
    context.update(each_context())

    tasks = []
    for group in component_group.get_ancestors(ascending=True, include_self=True):
        for task in group.task_set.all():
            task.last_done = task_last_done_date(task, group)
            # task.next_due = task.next_due_date(component_group)
            task.group = group
            tasks += [task]

    context.update({"tasks": tasks})

    return render(request, 'taskmonitor/taskmonitor_component_detail.html', context)
