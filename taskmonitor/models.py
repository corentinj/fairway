from django.db import models
from django.utils.decorators import classonlymethod
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager

# TODO How to handle exception in Task.last_done_date()


class ComponentGroup(MPTTModel):
    name = models.CharField(max_length=100)
    full_name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    icon = models.CharField(max_length=100, null=True, blank=True)
    parent = TreeForeignKey('self', related_name='children', null=True, blank=True)
    tree = TreeManager()

    # @property
    def __str__(self):
        return self.name

    # def is_indirect_child(self, group):
    def is_sub_menu(self):
        return self.level == 1


class Component(ComponentGroup):
    manufacturer = models.CharField(max_length=100, null=True, blank=True)
    model = models.CharField(max_length=100, null=True, blank=True)


class Lifeboat(Component):
    capacity = models.PositiveSmallIntegerField()


class Task(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    interval = models.DurationField(blank=True, null=True)
    component_groups = models.ManyToManyField(ComponentGroup)

    def __str__(self):
        return self.name


class TaskLogEntry(models.Model):
    task = models.ForeignKey(Task)
    date = models.DateField()
    remark = models.TextField(null=True, blank=True)
    component_group = models.ForeignKey(ComponentGroup)

    def __str__(self):
        return self.component_group.name + ' / ' + self.task.name + ' / ' + str(self.date)
