# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ComponentGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=100)),
                ('full_name', models.CharField(blank=True, max_length=100, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('interval', models.DurationField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TaskLogEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('date', models.DateField()),
                ('remark', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Component',
            fields=[
                ('componentgroup_ptr', models.OneToOneField(auto_created=True, primary_key=True, parent_link=True, to='taskmonitor.ComponentGroup', serialize=False)),
                ('manufacturer', models.CharField(blank=True, max_length=100, null=True)),
                ('model', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('taskmonitor.componentgroup',),
        ),
        migrations.AddField(
            model_name='tasklogentry',
            name='component_group',
            field=models.ForeignKey(to='taskmonitor.ComponentGroup'),
        ),
        migrations.AddField(
            model_name='tasklogentry',
            name='task',
            field=models.ForeignKey(to='taskmonitor.Task'),
        ),
        migrations.AddField(
            model_name='task',
            name='component_groups',
            field=models.ManyToManyField(to='taskmonitor.ComponentGroup'),
        ),
        migrations.AddField(
            model_name='componentgroup',
            name='parent',
            field=mptt.fields.TreeForeignKey(null=True, to='taskmonitor.ComponentGroup', related_name='children', blank=True),
        ),
        migrations.CreateModel(
            name='Lifeboat',
            fields=[
                ('component_ptr', models.OneToOneField(auto_created=True, primary_key=True, parent_link=True, to='taskmonitor.Component', serialize=False)),
                ('capacity', models.PositiveSmallIntegerField()),
            ],
            options={
                'abstract': False,
            },
            bases=('taskmonitor.component',),
        ),
    ]
