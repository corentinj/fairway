from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import *

# Register your models here.

admin.site.register(ComponentGroup, MPTTModelAdmin)
admin.site.register(Component)
admin.site.register(Lifeboat)
admin.site.register(Task)
admin.site.register(TaskLogEntry)
